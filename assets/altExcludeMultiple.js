/**
 * @file altExcludeMultiple javascript function
 * @author Denis Chenu
 * @copyright Denis Chenu <http://www.sondages.pro>
 * @license magnet:?xt=urn:btih:0b31508aeb0634b347b8270c7bee4d411b5d4109&dn=agpl-3.0.txt AGPL v3.0
 */

function altExcludeMultiple(qid,exclude) {
  $("#question"+qid+" .checkbox-item :checkbox").on('change',function() {
    if($(this).is(":checked")) {
      if($(this).attr('id')==exclude) {
        $(this).closest(".subquestion-list").find(".checkbox-item :checkbox").not("#"+exclude).filter(":checked").trigger('click');
      } else {
        if($("#"+exclude).is(":checked")) {
          $("#"+exclude).trigger('click');
        }
      }
    }
  });
  if($("#"+exclude).is(":checked")) {
    $("#"+exclude).closest(".subquestion-list").find(".checkbox-item :checkbox").not("#"+exclude).filter(":checked").trigger('click');
  }
}
