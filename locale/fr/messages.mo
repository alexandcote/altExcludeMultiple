��          4      L       `   �   a   -   1  �  _  �     9                       Excludes all other options if a certain answer is selected. Ensures that other responses are “not selected” in the database rather than “null”. Just enter the answer code(s) separated by a semicolon. Exclusive option (other choices not selected) Project-Id-Version: altExcludeMultiple
POT-Creation-Date: 2017-09-01 08:40+0200
PO-Revision-Date: 2017-09-01 18:50+0200
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.3
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: _translate
Last-Translator: 
Language: fr
X-Poedit-SearchPath-0: .
 Exclure toutes les autres options si certaines réponses sont sélectionnées. Fait en sorte que les autres réponses sont à «non sélectionné» dans la base de données plutôt que «null». Entrez les codes séparés par des points-virgules. Option d‘exclusivité (autres choix non sélectionnés) 